PySpark application for generating section image recommendations.

## Extracting images from articles

Example invocation to extract images per section:

```
$ spark2-submit --master yarn imagerec/article_images.py --wikitext-snapshot 2022-05 --inter-page-link-snapshot 2022-05-30 --output /user/mnz/output/article_images.parquet --wp-codes en pt
```
