def wp_code_to_wiki_db(wp_code: str) -> str:
    return wp_code.replace("-", "_") + "wiki"
